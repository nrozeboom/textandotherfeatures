import pandas as pd

def getColumnList(my_df, target):
    X_columns = my_df.columns.tolist() 
    X_columns = [x for x in X_columns if x != target]
    X_columns = [x for x in X_columns if x != 'Risks']
    X_columns = [x for x in X_columns if x != 'Mitigation']
    return(X_columns)
