import pandas as pd
import re
from sklearn.preprocessing import LabelEncoder

# get all the Target categories (label encode)
def getInitialCategories(my_df, target):
    targetc_list = my_df[target].tolist()
    le = LabelEncoder()
    le.fit(targetc_list)
    target_cat = list(le.classes_)
    return(target_cat)

def getGroupedCategories(my_df, target):
    targetc_groupby = my_df.groupby(target).count()
    return(targetc_groupby)

# remove all features with data below this threshold
def removeFeatures(DATA_THRESHOLD, my_df):    
    columns = list(my_df)
    for i in range(1, len(columns)):
        num_nan_temp = my_df.loc[ (pd.isna(my_df[columns[i]])) , columns[i]].shape[0]
        if round((num_nan_temp / my_df.shape[0] * 100),0) > (100-DATA_THRESHOLD):
            my_df = my_df.drop([columns[i]], axis=1)
    return(my_df)

# create dataframe of Target with amount of data it contains and threshold value
def createDataframeTarget(THRESHOLD, my_df, target):
    # get all the Target categories (label encode) 
    target_cat = getInitialCategories(my_df, target)

    targetc_groupby = getGroupedCategories(my_df, target)
    counts = []    
    for i in range(0,len(target_cat)-1):
        counts.append([target_cat[i], targetc_groupby['Segment'][i], THRESHOLD])   

    counts = pd.DataFrame(counts, columns=[target,'counts', 'threshold']).sort_values(by=['counts'])
    return(counts)

# only take data from Target above defined threshold value
def getDataAboveTreshhold(THRESHOLD, my_df, target):
    # get all the Target categories (label encode) 
    target_cat = getInitialCategories(my_df, target)

    targetc_groupby = getGroupedCategories(my_df, target)
    for i in range(0, len(target_cat)-1):
        if (targetc_groupby['Segment'][i] > THRESHOLD):        
            my_df = my_df[my_df[target] != '13 tbd (to be determined)']
        if (targetc_groupby['Segment'][i] < THRESHOLD):        
            my_df = my_df[my_df[target] != target_cat[i]] 
    return(my_df)

# get all the Target categories and Label Encode
def getTargetCategoriesAndCounts(my_df, target):
    nccc_list = my_df[target].tolist()
    le = LabelEncoder()
    le.fit(nccc_list)
    target_cat = list(le.classes_)
    target_cat_nr = le.transform(list(le.classes_))
 
    # get value counts of Target per category
    value_counts = my_df[target].value_counts()
    value_counts = value_counts.rename_axis('categories').reset_index(name='counts')

    # get all remaning Target categories (script will not run under every circumstance)
    categories = []
    for i in range(0, len(target_cat)-1):
        counts = value_counts.loc[value_counts['categories'] == target_cat[i]]
        y = re.compile('\d{3,4}')
        counts = y.findall(str(counts))   
        categories.append([target_cat_nr[i], target_cat[i], int(counts[0])])      
    categories = pd.DataFrame(categories)
    categories.columns = ['number', 'categories', 'counts']
    return(categories, counts)

