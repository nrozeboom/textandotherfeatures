import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt

def percAvailableData(DATA_THRESHOLD, my_df):
    # create dataframe of features with percentage of data it contains and threshold value
    filled = [];
    columns = list(my_df)
    for i in range(0, len(columns)):
        num_nan_temp = my_df.loc[(pd.isna(my_df[columns[i]])) , columns[i]].shape[0]
        filled.append([columns[i], (100 - round((num_nan_temp / my_df.shape[0] * 100), 0)), DATA_THRESHOLD])     
    filled = pd.DataFrame(filled, columns=['features','% filled', 'threshold']).sort_values(by=['% filled'])
    
    plt.figure(figsize=(15,5))

    # show percentage of available data per feature with threshold value
    g = sns.lineplot(x="features", y="threshold", data=filled, color="g"  )
    h = sns.barplot(x="features", y="% filled", data=filled, color="b" )
    
    # show percentage of available data per feature with threshold value
    for label in h.get_xticklabels():
        label.set_rotation(90) 
    return(h)

def percAvailableDataFeature(counts, target):
    # show percentage of available data per feature with threshold value
    g = sns.lineplot(x=target, y="threshold", data=counts, color="g"  )
    h = sns.barplot(x=target, y="counts", data=counts, color="b" )
    for label in h.get_xticklabels():
        label.set_rotation(90)
    return(h)


def createConfusionMatrix(y_test,predicted, my_df, target, metrics):  
    df_cm = pd.DataFrame(
        metrics.confusion_matrix(y_test,predicted), index=list(set(my_df[target])), columns=list(set(my_df[target])) 
    )
    sns.heatmap(df_cm, cmap='Greens', cbar=True, fmt="d", annot=True, square=True, annot_kws={'size': 7}, yticklabels=list(set(my_df[target])), xticklabels=list(set(my_df[target])))
    plt.ylabel('True')
    plt.xlabel('Predicted')
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 6
    fig_size[1] = 6
    plt.rcParams["figure.figsize"] = fig_size
    plt.show()
