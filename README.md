# textAndOtherFeatures

This project tries to predict a certain category. 
Number of categories are more than 10.
Features exist out of text and other kind of features.
This project is about the strategy of combining different kind of features with the help of pipelines


**pre-requisites to run this project**
1. TestDataNC.xls: This file contains several features. 
   Both features "Title" and "Detailed_Description" contain text. These 2 seperate values will be merged to one.
   Other features are: PID and Segment. Segment should always contain a value!
   PID should start with a country ISO code.
2. drop.txt: This file contains a list of features that will be deleted
3. target.txt: This file contains the target label
4. Example of TestDataNC.xls:
   <table>
     <tr>
       <td>Title</td><td>Detailed_Description</td><td>PID</td><td>Segment</td><td>Target</td>
     </tr>
     <tr>
       <td>nice weather</td><td>today it is sunny with about 30 degrees</td><td>DE</td><td>Bayern</td><td>Summer</td>
     </tr>
     <tr>
       <td>cold weather</td><td>today a lot of snow will fall</td><td>CH</td><td>Bern</td><td>Winter</td>
     </tr>
   </table>


